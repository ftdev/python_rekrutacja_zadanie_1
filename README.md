# Focus Telecom Polska
### Zadanie rekrutacyjne numer 1

Dostarczony kod to realizacja Gry w życie. Reguły gry a tym samym algorytm są [dostępne na Wikipedii](https://pl.wikipedia.org/wiki/Gra_w_%C5%BCycie). 

Program znajdujący się w pliku *main.py* zawiera błędy. Zadanie polega na namierzeniu błędów i poprawieniu ich.  